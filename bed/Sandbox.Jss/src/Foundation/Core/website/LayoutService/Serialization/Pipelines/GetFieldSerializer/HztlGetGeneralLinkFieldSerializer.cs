using Sandbox.Foundation.Core.LayoutService.Serialization.FieldSerializers;
using Sitecore.Diagnostics;
using Sitecore.LayoutService.Serialization;
using Sitecore.LayoutService.Serialization.Pipelines.GetFieldSerializer;

namespace Sandbox.Foundation.Core.LayoutService.Serialization.Pipelines.GetFieldSerializer
{
    public class HztlGetGeneralLinkFieldSerializer : GetGeneralLinkFieldSerializer
    {
        public HztlGetGeneralLinkFieldSerializer(IFieldRenderer fieldRenderer)
            : base(fieldRenderer)
        {
        }

        protected override void SetResult(GetFieldSerializerPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));
            args.Result = new HztlGeneralLinkFieldSerializer(FieldRenderer);
        }
    }
}
