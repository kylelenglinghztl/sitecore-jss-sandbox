using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.LayoutService.Configuration;
using Sitecore.Mvc.Presentation;

namespace Sandbox.Foundation.Core.LayoutService.ItemRendering.ContentsResolvers
{
    [UsedImplicitly]
    public class RenderingContentsWithChildrenResolver : HztlRenderingContentsResolverBase
    {

        public override object ResolveContents(
          Rendering rendering,
          IRenderingConfiguration renderingConfig)
        {
            Assert.ArgumentNotNull(rendering, nameof(rendering));
            Assert.ArgumentNotNull(renderingConfig, nameof(renderingConfig));
            Item contextItem = GetContextItem(rendering, renderingConfig);

            if (contextItem == null)
            {
                return null;
            }

            return ProcessItem(contextItem, rendering, renderingConfig);
        }

        protected override JObject ProcessItem(
          Item item,
          Rendering rendering,
          IRenderingConfiguration renderingConfig)
        {
            Assert.ArgumentNotNull(item, nameof(item));

            if (!int.TryParse(Parameters["depth"], out int depth))
            {
                depth = 0;
            }

            using (new SettingsSwitcher("Media.AlwaysIncludeServerUrl", IncludeServerUrlInMediaUrls.ToString()))
            {
                var root = new JObject
                {
                    ["root"] = GetItemJson(item, renderingConfig.ItemSerializer, depth)
                };
                return root;
            }
        }
    }
}
