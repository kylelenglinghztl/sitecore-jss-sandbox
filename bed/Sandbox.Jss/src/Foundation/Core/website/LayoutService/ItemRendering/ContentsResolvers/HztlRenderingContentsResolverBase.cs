using System.Linq;
using Newtonsoft.Json.Linq;
using Sitecore.Data.Items;
using Sitecore.LayoutService.Helpers;
using Sitecore.LayoutService.ItemRendering.ContentsResolvers;
using Sitecore.LayoutService.Serialization.ItemSerializers;
using Sitecore.Links;

namespace Sandbox.Foundation.Core.LayoutService.ItemRendering.ContentsResolvers
{
    public class HztlRenderingContentsResolverBase : RenderingContentsResolver
    {
        public static JObject GetItemJson(Item item, IItemSerializer renderingConfigItemSerializer, int depth = 0)
        {
            var fields = JObject.Parse(renderingConfigItemSerializer.Serialize(item));
            var obj = new JObject
            {
                ["id"] = item.ID.Guid.ToString(),
                ["templateId"] = item.TemplateID.Guid.ToString(),
                ["url"] = LinkManager.GetItemUrl(item, ItemUrlHelper.GetLayoutServiceUrlOptions()),
                ["name"] = item.Name,
                ["displayName"] = item.DisplayName,
                ["fields"] = fields
            };

            if (depth > 0)
            {
                var children = item.Children.Select(x => GetItemJson(x, renderingConfigItemSerializer, depth - 1)).ToList();

                obj["children"] = JArray.FromObject(children);
            }

            return obj;
        }
    }
}
