
using System;
using JetBrains.Annotations;
using StackExchange.Redis;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Sandbox.Foundation.Core.App_Start.Startup), "PreStart")]

namespace Sandbox.Foundation.Core.App_Start
{
    public static class Startup
    {
        [UsedImplicitly]
        public static void PreStart()
        {
            // Hopefully fix random issue described here: https://stackexchange.github.io/StackExchange.Redis/ThreadTheft
            ConnectionMultiplexer.SetFeatureFlag("preventthreadtheft", true);
        }
    }
}
