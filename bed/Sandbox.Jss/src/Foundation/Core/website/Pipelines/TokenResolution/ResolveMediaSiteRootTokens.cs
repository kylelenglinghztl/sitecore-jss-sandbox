using System.Linq;
using JetBrains.Annotations;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Multisite.Extensions;
using Sitecore.XA.Foundation.TokenResolution.Pipelines.ResolveTokens;

namespace Sandbox.Foundation.Core.Pipelines.TokenResolution
{
    [UsedImplicitly]
    public class ResolveMediaSiteRootTokens : ResolveTokensProcessor
    {
        private const string Token = "$mediaSiteRoot";
        private readonly IMultisiteContext _multisiteContext;

        public ResolveMediaSiteRootTokens(IMultisiteContext multisiteContext)
        {
            _multisiteContext = multisiteContext;
        }

        public override void Process(ResolveTokensArgs args)
        {
            args.Query = ReplaceTokenWithItemPath(args.Query, Token, () => GetMediaRoot(args.ContextItem), args.EscapeSpaces);
        }

        private Item GetMediaRoot(Item contextItem)
        {
            var mediaRoot = _multisiteContext.GetSiteMediaItem(contextItem);
            if (mediaRoot == null)
            {
                Log.Info($"ResolveMediaSiteRootTokens: Media root not found for context item ${contextItem.Paths.FullPath}", this);
                return null;
            }

            var root = mediaRoot.GetVirtualChildren();
            
            var mediaRootFirst = root.Any() ? root.First() : mediaRoot;

            return mediaRootFirst;
        }
    }
}
