using System.Linq;
using Sandbox.Foundation.Core.Services.ItemServices;
using Sandbox.Foundation.Core.Utilities;
using JetBrains.Annotations;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.ItemProvider.AddFromTemplate;

namespace Sandbox.Foundation.Core.Pipelines.ItemProvider.AddFromTemplate
{
    /// <summary>
    /// Augments the functionality of Branch Templates by making any rendering data sources set in the layout on the branch
    /// that point to other children of the branch be repointed to the newly created branch item
    /// instead of the source branch item. This allows for templating including data source items using branches.
    /// </summary>
    [UsedImplicitly]
    public class AddFromBranchRelink : AddFromTemplateProcessor
    {
        private readonly IItemRelinkService _itemRelinkService;

        public AddFromBranchRelink(IItemRelinkService itemRelinkService)
        {
            _itemRelinkService = itemRelinkService;
        }

        public override void Process(AddFromTemplateArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));

            if (AddFromBranchRelinkDisabler.IsActive)
            {
                return;
            }

            if (args.Destination.Database.Name != "master")
            {
                return;
            }

            var templateItem = args.Destination.Database.GetItem(args.TemplateId);

            Assert.IsNotNull(templateItem, "Template did not exist!");

            // if this isn't a branch template, we can use the stock behavior
            if (templateItem.TemplateID != TemplateIDs.BranchTemplate)
            {
                return;
            }

            Assert.HasAccess((args.Destination.Access.CanCreate() ? 1 : 0) != 0, "AddFromTemplate - Add access required (destination: {0}, template: {1})", args.Destination.ID, args.TemplateId);

            using (new AddFromBranchRelinkDisabler())
            {
                // Create the branch template instance
                var newItem = args.Destination.Database.Engines.DataEngine.AddFromTemplate(args.ItemName, args.TemplateId,
                    args.Destination, args.NewId);

                var sourceItem = ((BranchItem)templateItem).InnerItem.Children.FirstOrDefault();

                _itemRelinkService.RelinkItemReferencesToChildren(newItem, sourceItem);

                args.Result = newItem;
            }
        }
    }

    public sealed class AddFromBranchRelinkDisabler : Disabler<AddFromBranchRelinkDisabler>
    {
    }
}
