using System;
using System.Collections.Generic;
using System.Linq;
using Sandbox.Foundation.Core.Services.Wrappers;
using Sandbox.Foundation.DI.Attributes;
using Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes;
using Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.SecurityModel;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;

namespace Sandbox.Foundation.Core.Services.ItemServices
{
    public interface ILastUpdatedService
    {
        void SetPageLastUpdated(Item item);
    }

    [ServiceImplementation(typeof(ILastUpdatedService))]
    public class LastUpdatedService : ILastUpdatedService
    {
        private readonly ISitecoreGlobals _sitecoreGlobals;
        private readonly IDateUtilWrapper _dateUtilWrapper;
        private readonly ITemplateHelper _templateHelper;

        public LastUpdatedService(ISitecoreGlobals sitecoreGlobals, IDateUtilWrapper dateUtilWrapper, ITemplateHelper templateHelper)
        {
            _sitecoreGlobals = sitecoreGlobals;
            _dateUtilWrapper = dateUtilWrapper;
            _templateHelper = templateHelper;
        }
        /// <summary>
        /// Static list of current items being processed across all threads
        /// </summary>
        private static readonly SynchronizedCollection<ID> AllInProcess = new SynchronizedCollection<ID>();

        /// <summary>
        /// List of current items being processed in this instance
        /// </summary>
        private readonly SynchronizedCollection<ID> _pagesToProcess = new SynchronizedCollection<ID>();

        public void SetPageLastUpdated(Item item)
        {
            Assert.ArgumentNotNull(item, nameof(item));

            _pagesToProcess.Clear();

            SetPagesToProcess(item);

            var db = item.Database;

            var pages = _pagesToProcess.Select(x => (BasePage)db.GetItem(x)).ToList();

            foreach (var basePage in pages)
            {
                try
                {
                    using (new EditContext(basePage, SecurityCheck.Disable))
                    {
                        basePage.LastUpdatedField.Value = _dateUtilWrapper.IsoNow;
                    }
                }
                catch (Exception ex)
                {
                    basePage.InnerItem.Editing.CancelEdit();
                    Log.Error($"Unable to set lastUpdated field for {basePage.InnerItem.Paths.FullPath}", ex, this);
                }
                finally
                {
                    // Remove from list regardless of whether success or failure
                    AllInProcess.Remove(basePage.ID);
                }
            }
        }

        private void SetPagesToProcess(Item item)
        {
            // check to see if this item was already just saved, if so exit to prevent infinite loop.  
            if (AllInProcess.Contains(item.ID))
            {
                return;
            }

            // Get all links
            var links = _sitecoreGlobals.LinkDatabase.GetReferrers(item);

            // Get the links that are pages
            var linkedPages = links?.Select(i => i.GetSourceItem())
                .Where(i => i != null && i.Versions.IsLatestVersion() && _templateHelper.ItemInheritsFrom(i,BasePage.ItemTemplateId))
                .ToList() ?? new List<Item>();

            if (!linkedPages.Any(x=>x.ID == item.ID) && _templateHelper.ItemInheritsFrom(item, BasePage.ItemTemplateId))
            {
                linkedPages.Add(item);
            }
            // Add them to our lists
            foreach (var page in linkedPages)
            {
                _pagesToProcess.Add(page.ID);
                AllInProcess.Add(page.ID);
            }

            // If the parent of this item considers its children part of itself, check on the parent too.
            if (_templateHelper.ItemInheritsFrom(item.Parent, WorkflowIncludeChildrenFlag.ItemTemplateId))
            {
                SetPagesToProcess(item.Parent);
            }
        }
    }
}
