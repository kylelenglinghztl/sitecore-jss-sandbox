using System;
using System.Linq;
using Sandbox.Foundation.Core.Utilities;
using Sandbox.Foundation.DI.Attributes;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.SecurityModel;
using Sitecore.StringExtensions;

namespace Sandbox.Foundation.Core.Services.ItemServices
{
    public interface IItemRelinkService
    {
        void RelinkItemReferencesToChildren(Item newItem, Item sourceItem);
    }

    [ServiceImplementation(typeof(IItemRelinkService))]
    public class ItemRelinkService : IItemRelinkService
    {
        public void RelinkItemReferencesToChildren(Item newItem, Item sourceItem)
        {
            // Relink relative rendering datasources
            RewriteBranchRenderingDataSources(newItem, sourceItem, newItem);

            // Relink relative field values
            RewriteBranchRelativeFieldTargets(newItem, sourceItem, newItem);
        }


        public virtual void RewriteBranchRelativeFieldTargets(Item newItem, Item sourceItem, Item newRootItem)
        {
            Assert.ArgumentNotNull(newItem, $"{nameof(newItem)} is null.");
            Assert.ArgumentNotNull(sourceItem, $"{nameof(sourceItem)} is null.");
            Assert.ArgumentNotNull(newRootItem, $"{nameof(newRootItem)} is null.");

            var sourceItemPath = sourceItem.Paths.FullPath;

            foreach (Field field in newItem.Fields)
            {
                var linkField = new InternalLinkField(field);
                var targetItem = linkField.TargetItem;

                // if there was no valid target item OR the target item is not a child of the branch template we skip out
                if (targetItem == null)
                {
                    continue;
                }

                var newTargetItem = GetNewTargetItem(newItem, targetItem, sourceItemPath);

                // This should not happen, but in case it is a relative path and we are not able to resolve
                // a new item, set it to empty to prevent linking to the wrong item.
                if (newTargetItem == null)
                {
                    Log.Error($"{this}: New Target item could not be found", this);
                    linkField.Value = "INVALID_BRANCH_SUBITEM_ID";
                    continue;
                }

                if (newTargetItem == targetItem)
                {
                    continue;
                }

                using (new SecurityDisabler())
                using (new EditContext(newItem))
                {
                    linkField.Path = newTargetItem.ID.ToString();
                }
            }

            if (!newItem.HasChildren)
            {
                return;
            }

            newItem.Children.ToList().ForEach(x => RewriteBranchRelativeFieldTargets(x, sourceItem.Database.GetItem(sourceItem.Paths.FullPath + "/" + x.Name), newRootItem));
        }

        public virtual void RewriteBranchRenderingDataSources(Item newItem, Item sourceItem, Item newRootItem)
        {
            Assert.ArgumentNotNull(newItem, $"{nameof(newItem)} is null.");
            Assert.ArgumentNotNull(sourceItem, $"{nameof(sourceItem)} is null.");
            Assert.ArgumentNotNull(newRootItem, $"{nameof(newRootItem)} is null.");

            var sourceItemPath = sourceItem.Paths.FullPath;

            LayoutHelper.ApplyActionToAllRenderings(newItem, rendering =>
            {
                if (string.IsNullOrWhiteSpace(rendering.Datasource))
                {
                    return RenderingActionResult.None;
                }

                // note: queries and multiple item datasources are not supported
                var targetItem = newItem.Database.GetItem(rendering.Datasource);
                if (targetItem == null)
                {
                    Log.Warn("Error while expanding branch template rendering datasources: data source {0} was not resolvable."
                        .FormatWith(rendering.Datasource), this);

                    return RenderingActionResult.None;
                }

                var newTargetItem = GetNewTargetItem(newRootItem, targetItem, sourceItemPath);

                // if the target item was a valid under branch item, but the same relative path does not exist under the branch instance
                // we set the datasource to something invalid to avoid any potential unintentional edits of a shared data source item
                if (newTargetItem == null)
                {
                    Log.Error($"{this}: New Target item could not be found", this);
                    rendering.Datasource = "INVALID_BRANCH_SUBITEM_ID";
                    return RenderingActionResult.None;
                }
                // If it is the same item, don't change the datasource
                if (newTargetItem == targetItem)
                {
                    return RenderingActionResult.None;
                }

                rendering.Datasource = newTargetItem.ID.ToString();
                return RenderingActionResult.None;
            });

            if (!newItem.HasChildren)
            {
                return;
            }

            newItem.Children.ToList().ForEach(x => RewriteBranchRenderingDataSources(x, sourceItem, newRootItem));
        }

        private Item GetNewTargetItem(Item item, Item targetItem, string branchBasePath)
        {
            // if there was no valid target item OR the target item is not a child of the branch template we skip out
            if (!targetItem.Paths.FullPath.StartsWith(branchBasePath, StringComparison.OrdinalIgnoreCase))
            {
                return targetItem;
            }

            var relativeItemPath = targetItem.Paths.FullPath.Substring(branchBasePath.Length);

            var newTargetPath = item.Paths.FullPath + relativeItemPath;

            var newTargetItem = item.Database.GetItem(newTargetPath);

            return newTargetItem;
        }
    }
}
