using System.Net.Http;
using Sandbox.Foundation.DI.Attributes;
using Newtonsoft.Json;
using Sitecore.Diagnostics;

namespace Sandbox.Foundation.Core.Services.Wrappers
{
    public interface IHttpClientWrapper
    {
        T GetJson<T>(string url) where T: class;
    }

    [ServiceImplementation(typeof(IHttpClientWrapper))]
    public class HttpClientWrapper : IHttpClientWrapper
    {
        private readonly HttpClient _implementation;

        public HttpClientWrapper()
        {
            _implementation = new HttpClient();
        }

        public T GetJson<T>(string url) where T: class
        {

            var response = _implementation.GetAsync(url).Result;

            if (!response.IsSuccessStatusCode)
            {
                Log.Warn(JsonConvert.SerializeObject(response), this);
                return null;
            }

            return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
        }
    }
}
