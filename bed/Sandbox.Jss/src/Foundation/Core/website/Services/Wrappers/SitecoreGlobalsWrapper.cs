using Sandbox.Foundation.DI.Attributes;
using JetBrains.Annotations;
using Sitecore;
using Sitecore.Links;

namespace Sandbox.Foundation.Core.Services.Wrappers
{
    public interface ISitecoreGlobals
    {
        LinkDatabase LinkDatabase { get; }
    }

    [ServiceImplementation(typeof(ISitecoreGlobals))]
    [UsedImplicitly]
    public class SitecoreGlobalsWrapper : ISitecoreGlobals
    {
        public LinkDatabase LinkDatabase => Globals.LinkDatabase;
    }
}
