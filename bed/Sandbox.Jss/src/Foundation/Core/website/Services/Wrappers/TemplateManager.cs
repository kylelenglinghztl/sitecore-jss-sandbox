using Sandbox.Foundation.DI.Attributes;
using JetBrains.Annotations;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;

namespace Sandbox.Foundation.Core.Services.Wrappers
{
    public interface ITemplateHelper
    {
        bool ItemInheritsFrom(Item item, ID templateId);
    }

    [ServiceImplementation(typeof(ITemplateHelper))]
    [UsedImplicitly]
    public class TemplateHelper : ITemplateHelper
    {
        public bool ItemInheritsFrom(Item item, ID templateId)
        {
            return item.InheritsFrom(templateId);
        }
    }
}
