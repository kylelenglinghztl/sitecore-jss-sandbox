using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Sandbox.Foundation.Orm.Models.Foundation.Core.BaseTemplates.Routes;
using Sandbox.Foundation.Orm.Models.Foundation.Core.Workflow;
using JetBrains.Annotations;
using Sitecore;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.ExperienceEditor.Utils;
using Sitecore.Workflows.Simple;
using Sitecore.XA.Foundation.Editing;
using Sitecore.XA.Foundation.Editing.Workflow;
using Sitecore.XA.Foundation.SitecoreExtensions.Extensions;

namespace Sandbox.Foundation.Core.Editing.Workflow
{
    [UsedImplicitly]
    public class ApplyForDataSourceExtendedItemsAction : ApplyForDataSourceItemsAction
    {
        private static ConcurrentSet<ID> _currentlyProcessing = new ConcurrentSet<ID>();

        /// <summary>
        /// Override the base functionality to exit out if any related item's workflow command fails.
        /// </summary>
        /// <param name="args"></param>
        [UsedImplicitly]
        public new void Process(WorkflowPipelineArgs args)
        {
            var innerItem = args.ProcessorItem.InnerItem;
            if (innerItem == null)
            {
                return;
            }

            if (_currentlyProcessing.Contains(innerItem.ID))
            {
                return;
            }

            _currentlyProcessing.Add(innerItem.ID);

            try
            {
                var itemValidationRuleValue = innerItem[Templates.DatasourceWorkflowAction.Fields.ItemValidationRule];
                if (ShouldProcess(args.DataItem, innerItem, itemValidationRuleValue))
                {
                    var renderingDataSourceItems = GetRenderingDataSourceItems(args.DataItem);

                    var customCommandItem = GetCustomCommandItem(args);

                    var datasourceValidationRuleValue =
                        innerItem[Templates.DatasourceWorkflowAction.Fields.DatasourceValidationRule];

                    foreach (var filterSameItem in
                        ItemUtility.FilterSameItems(renderingDataSourceItems))
                    {
                        if (filterSameItem.Access.CanWrite() &&
                            (!filterSameItem.Locking.IsLocked() || filterSameItem.Locking.HasLock()) &&
                            ShouldProcess(innerItem, filterSameItem, datasourceValidationRuleValue))
                        {
                            var workflowResult = WorkflowUtility.ExecuteWorkflowCommandIfAvailable(filterSameItem,
                                customCommandItem ?? args.CommandItem, args.CommentFields);

                            // If there is a result and it isn't successful.  If the result is null that is fine,
                            // it means the command isn't available for the current workflow state
                            if (workflowResult != null && !workflowResult.Succeeded)
                            {
                                // If any related item workflow failed (e.g. validation failed) abort the pipeline
                                args.AbortPipeline();
                                return;
                            }
                        }
                    }
                }
            }
            finally
            {
                _currentlyProcessing.Remove(innerItem.ID);
            }
        }

        protected override List<Item> GetRenderingDataSourceItems(Item item)
        {
            // When in the context of a Sitecore command, the Context.Device is null
            // This causes a null ref in base.GetRenderingDataSourceItems(item).
            // To prevent that, we set the device to the default
            // Note, DeviceSwitcher doesn't work because it throws an exception when trying to 
            // restore the original value of null.
            if (Context.Device == null)
            {
                Context.Device = GetDefaultDevice(item.Database);
            }

            var baseItems = base.GetRenderingDataSourceItems(item);

            var results = RecursivelyGetValidChildren(baseItems).ToList();

            return results;
        }

        private static DeviceItem GetDefaultDevice(Database itemDatabase)
        {
            var innerItem = itemDatabase.GetItem("{FE5D7FDF-89C0-4D99-9AA3-B5FBD009C9F3}");
            return innerItem != null ? new DeviceItem(innerItem) : null;
        }

        private IEnumerable<Item> RecursivelyGetValidChildren(IEnumerable<Item> items)
        {
            foreach (var item in items)
            {
                // Don't include pages, we don't want a parent page to publish child pages
                if (item.Template.DoesTemplateInheritFrom(BaseRoute.ItemTemplateId))
                {
                    continue;
                }
                // Unlock the item if we can
                if (item.Locking.CanUnlock())
                {
                    Log.Info($"Unlocking: {item.Paths.FullPath}", this);
                    item.Locking.Unlock();
                }
                else
                {
                    Log.Info($"Cannot unlock: {item.Paths.FullPath}", this);
                }

                yield return item;

                if (item.Template.DoesTemplateInheritFrom(WorkflowIncludeChildrenFlag.ItemTemplateId))
                {
                    foreach (var child in RecursivelyGetValidChildren(item.Children))
                    {
                        yield return child;
                    }
                }
            }
        }
    }
}
